== Famingle (Family-Mingle)

= Challenge

Everyone wants to know their family tree. The biggest issue right now is that people don't have a great place to manage those connections and keep track as they find out more about their family. Famingle allows you to keep track of those family members in a simple, easy-to-use application.

= First Thoughts

In order to save your family we're going to need a few models:

- Users: To allow users to save their family tree and come back to visit it later.
- UserSessions: Allow users to sign in.
- People: Users can add people to their family tree.
- Relationship: Allow users to connect to each other in various ways
- RelationshipType: Allow for relationships to have a certain type (and allow that type to be reversible for future projects)

The most complicated issue we have here I think is how to handle relationships and which types of relationships we're going to need.

= The Setup

(NOTE: primary_key and timestamps are implied and not added to each)

1. Users
	- name:string
	- email:string
	- date_of_birth:datetime
	- :: Other Authlogic Fields for password_salt, etc.

2. UserSessions
	- :: Handled by Authlogic

3. People
	- user_id:integer
	- name:string
	- date_of_birth:datetime
	- is_user:boolean
	
4. Relationship
	- relationship_type_id:integer
	- person_id:integer
	- related_person_id:integer

5. RelationshipType
	- name:string
	- gender:string

Users have_many People
People have_many Relationships
Relationships have_one Person
Relationships have_one Connection (Connection is just the other Person)

English: Users create people and connect them using relationships.

== Future enhancements

The biggest thing I would do to enhance this is create a system that makes relationships automatically based on what you enter. For example, if you create a daughter of your mother, it would be safe to assume that that would then be your sister (or you could change it to half/step sister). Or, if you add your daughter and you've already added your son, it would create a relationship between the two of them (sister and brother). That system would allow your tree to grow very quickly and much more efficiently. It will also make sure that you don't duplicate and add two seperate people for one person.
Then, aside from better/more tests, there would be these things: 

1. Edit users
2. Allow users to upload a profile picture
3. User descriptions/bio
4. Make sure that users can never erase their own user-person.
5. Allow users to invite their family members via facebook/twitter.
6. Allow new users to "merge" with existing people in an existing tree. So in my case, if I make a tree and invite my brother, he can create an account and join my tree. That would replace his user on my tree with his user icon, etc. Their tree will also then be part of my tree.
