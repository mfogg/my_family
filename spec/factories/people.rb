FactoryGirl.define do
  factory :person do
    user_id 1
    date_of_birth 50.years.ago
    name "Test Person"
  end
end
