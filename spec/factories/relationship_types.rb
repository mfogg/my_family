# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :relationship_type do
    name "Mother"
    gender "male"
  end
end
