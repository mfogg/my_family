require 'spec_helper'

describe "People" do
  
  before do
    @user = FactoryGirl.create(:user)
  end
  describe "GET index" do
    describe "user not logged in" do
      it "should require you to be logged in" do
        visit user_people_path(@user)
        current_path.should == login_path
      end
    end
    
    describe "user logged in" do
      before do
        login
      end
      it "should display some people" do 
        @related_person = FactoryGirl.create(:person, :user_id => @user.id, :name => "Test Add", :parent_id => @user.me.id, :relationship_type => 1)
        visit user_people_path(@user)
        current_path.should == user_people_path(@user)
        page.should have_content @related_person.name
      end
      
      it "should allow you to click add on user" do
        #this should also cover the show page 
        visit user_people_path(@user)
        find("#person_#{@user.me.id}").click_link "Add"
        current_path.should == add_user_person_path(@user, @user.me)
      end
      
      it "should be able to delete a relationship" do
        @related_person = FactoryGirl.create(:person, :user_id => @user.id, :name => "Test Add", :parent_id => @user.me.id, :relationship_type => 1)
        visit user_people_path(@user)
        #save_and_open_page
        find("#person_#{@related_person.id}").click_link "Remove"
      end
    end
  end
  
  describe "GET add" do
    describe "user not logged in" do
      it "should require you to be logged in" do
        visit user_people_path(@user)
        current_path.should == login_path
      end
    end
    
    describe "user logged in" do
      before do
        login
      end
      it "should allow you to create a new person" do 
        visit add_user_person_path(@user, @user.me)
        current_path.should == add_user_person_path(@user, @user.me)
        
        fill_in "person_name", :with => @user.me.name
        select @user.date_of_birth.year.to_s, :from => "person_date_of_birth_1i"
        select Date::MONTHNAMES[@user.date_of_birth.month], :from => "person_date_of_birth_2i"
        select @user.date_of_birth.day.to_s, :from => "person_date_of_birth_3i"
        click_button "Create"
        
        page.should have_no_content "wrong"
      end
    end
  end
end
