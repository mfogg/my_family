require 'spec_helper'

describe "Relationships" do
  before do
    @user = FactoryGirl.create(:user)
    @person = FactoryGirl.create(:person, :user_id => @user.id)
  end
  
  describe "user not logged in" do
    describe "GET index" do
      it "should require you to be logged in" do
        visit user_person_relationships_path(@user, @person)
        current_path.should == login_path
      end
    end
  end
  
  describe "user logged in" do
    before do
      login
    end
    
    describe "GET index" do
      it "should show that persons relationships" do 
        visit user_person_relationships_path(@user, @person)
        current_path.should == user_person_relationships_path(@user, @person)
        page.should have_content "Test Person"
      end
    end
    
  end
end
