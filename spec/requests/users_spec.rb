require 'spec_helper'

describe "Users" do
  
  before do
    @user = FactoryGirl.build(:user)
  end
  
  describe "GET /signup" do
    it "should allow a user to sign up" do
      visit signup_path
      fill_in "user_name", :with => @user.name
      fill_in "user_email", :with => @user.email
      select @user.date_of_birth.year.to_s, :from => "user_date_of_birth_1i"
      select Date::MONTHNAMES[@user.date_of_birth.month], :from => "user_date_of_birth_2i"
      select @user.date_of_birth.day.to_s, :from => "user_date_of_birth_3i"
      select "Male", :from => "user_gender"
      fill_in "user_password", :with => @user.password
      fill_in "user_password_confirmation", :with => @user.password_confirmation
      click_button "signup_button"
      page.should have_content "successfull"
      page.should have_no_content "failed"
    end
    
    it "should force a unique email" do
      visit signup_path
      @user_first = FactoryGirl.create(:user)
      fill_in "user_name", :with => @user.name
      fill_in "user_email", :with => @user.email
      select @user.date_of_birth.year.to_s, :from => "user_date_of_birth_1i"
      select Date::MONTHNAMES[@user.date_of_birth.month], :from => "user_date_of_birth_2i"
      select @user.date_of_birth.day.to_s, :from => "user_date_of_birth_3i"
      fill_in "user_password", :with => @user.password
      fill_in "user_password_confirmation", :with => @user.password_confirmation
      click_button "signup_button"
      page.should have_content "already been taken"
    end
  end
  
  describe "GET show" do
    before do
      @user.save
    end
    
    describe "user not logged in" do
      it "should require you to be logged in" do
        visit user_path(@user)
        current_path.should == login_path
      end
    end
    
    describe "user logged in" do
      before do
        login
      end
    
      it "should display users person" do 
        visit user_people_path(@user)
        current_path.should == user_people_path(@user)
      end
    end
  end
  
end
