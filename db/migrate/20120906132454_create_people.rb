class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.integer :user_id, :null => false
      t.string  :gender,  :default => 'male'
      t.datetime :date_of_birth
      t.string :name
      t.boolean :is_user, :default => false

      t.timestamps
    end
  end
end
