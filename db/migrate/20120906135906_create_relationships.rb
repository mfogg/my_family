class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :relationship_type_id
      t.integer :person_id
      t.integer :related_person_id

      t.timestamps
    end
  end
end
