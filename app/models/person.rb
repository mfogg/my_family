class Person < ActiveRecord::Base
  attr_accessible :date_of_birth, :is_user, :name, :gender, :parent_id, :relationship_type
  attr_accessor :parent_id, :relationship_type
  
  belongs_to :user
  has_many :relationships, :dependent => :destroy
  has_many :reverse_relationships, :dependent => :destroy, :class_name => 'Relationship', :foreign_key => 'related_person_id', :dependent => :destroy
  
  before_create :define_gender
  after_create :create_relationship
  
  def define_gender
    r_type = RelationshipType.find_by_id(self.relationship_type)
    self.gender = r_type ? r_type.gender : 'male' #make it default to male just so we can make the graphics... eventually this would need to change
  end
  
  #I'm seperating this from doing it automatically incase I want to use it down the road
  def create_relationship
    relationship = Relationship.where(:person_id => self.parent_id, :related_person_id => self.id).first
    return if relationship
    Relationship.create(:person_id => self.parent_id, :related_person_id => self.id, :relationship_type_id => self.relationship_type)
  end
end
