class UserSession < Authlogic::Session::Base
  #just to help rspec use it
  extend ActiveModel::Naming
  
  #just adding a couple things here to make authlogic sessions work
  
  def to_key
     new_record? ? nil : [ self.send(self.class.primary_key) ]
  end
  
  def persisted?
    false
  end
end