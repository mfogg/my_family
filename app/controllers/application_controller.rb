class ApplicationController < ActionController::Base
  protect_from_forgery
  
  helper_method :current_user
  
  private
  
  def find_user
    @user = User.find(params[:user_id] || params[:id])
  end
  
  #some cool stuff that authlogic can do
  
  def require_user
    unless current_user
      respond_to do |format|
        format.html { redirect_to login_path, :notice => "You must be logged in to access this page" }
      end
      return false
    end
  end

  def require_no_user
    if current_user
      store_location
      redirect_to newsfeed_user_path(current_user.login)
      return false
    end
  end
  
  def current_user_session
    return @current_user_session if defined?(@current_user_session)
    @current_user_session = UserSession.find
  end
  
  def current_user
    return @current_user if defined?(@current_user)
    @current_user = current_user_session && current_user_session.user
  end
  
end
