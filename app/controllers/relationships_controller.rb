class RelationshipsController < ApplicationController
  
  before_filter :find_user
  before_filter :find_person
  before_filter :require_user
  
  def index
  end
  
  def destroy
    @relationship = Relationship.find(params[:relationship_id] || params[:id])
    respond_to do |format|
      if @relationship.destroy
        format.html { redirect_to :back} #, :notice => "The person has been successfully removed!"
      else
        format.html { redirect_to :back} #, :notice => 'Something went wrong.'
      end
    end
  end
  
  private
  
  def find_person
    @person = Person.find(params[:person_id] || params[:id])
  end
end
