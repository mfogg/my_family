class UsersController < ApplicationController
  
  before_filter :require_user, :only => [:show]
  
  def new
    @user = User.new
    respond_to do |format|
      format.html{render :layout => 'pages'}
    end
  end
  
  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @user.save
        format.html { redirect_to( user_people_path(@user), :notice => 'Registration successfull.') }
      else
        format.html { render :new, :notice => 'Registration failed'}
      end
    end
  end
  
  def show
    @user = User.find(params[:id])
    respond_to do |format|
      format.html { redirect_to user_people_path(@user)}
    end
  end
end
